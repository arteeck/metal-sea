﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;

namespace Metal_Sea
{
    public class RandomField
    {
        private static Random random = new Random();
        public static Cell[,] Make(int size, List<Ship> ships)
        {
            var field = FillField(size);
            var i = 0;
            while (i < ships.Count)
            {
                var currentShip = ships[i];
                var emptyCells = field.FindAllIndexes(x => x == Cell.Empty);
                var emptyCell = emptyCells[random.Next(emptyCells.Count)];
                var possiblePlacements = new List<Size>()
                {
                    new Size(0, currentShip.health - 1),
                    new Size(0, 1 - currentShip.health),
                    new Size(1 - currentShip.health, 0),
                    new Size(currentShip.health - 1, 0)
                };
                var allowedPlacements = GetAllowedPlacements(field, currentShip.health, emptyCell, possiblePlacements);
                if (allowedPlacements.Count == 0)
                    continue;
                var selectedPlacement = allowedPlacements[random.Next(allowedPlacements.Count)];
                foreach (var position in selectedPlacement)
                {
                    field[position.X, position.Y] = Cell.Occupied;
                    currentShip.positions.Add(position);
                }
                currentShip.DefineBorderCells(field);
                i++;
            }
            return field;
        }

        private static List<List<Point>> GetAllowedPlacements(Cell[,] field, int length, Point startPosition, List<Size> placements) =>
            placements
            .Select(x => FillPlacement(x, startPosition, field))
            .Where(x => x.Count == length)
            .ToList();

        private static List<Point> FillPlacement(Size direction, Point startPosition, Cell[,] field)
        {
            var result = new List<Point>();
            for (int i = 0; i <= Math.Abs(direction.Width); i++)
                for (int j = 0; j <= Math.Abs(direction.Height); j++)
                {
                    var nextPosition = new Point(startPosition.X + i * Math.Sign(direction.Width),
                        startPosition.Y + j * Math.Sign(direction.Height));
                    if (Game.InsideMap(nextPosition)
                        && field[nextPosition.X, nextPosition.Y] == Cell.Empty)
                    {
                        result.Add(nextPosition);
                    }
                    else
                        break;
                }
            return result;
        }

        private static Cell[,] FillField(int size)
        {
            var field = new Cell[size, size];
            for (int i = 0; i < size; i++)
                for (int j = 0; j < size; j++)
                    field[i, j] = Cell.Empty;
            return field;
        }
    }
}
