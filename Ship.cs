﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace Metal_Sea
{
    public class Ship
    {
        public int health;
        public HashSet<Point> positions = new HashSet<Point>();
        public HashSet<Point> borderCells = new HashSet<Point>();
        public PictureBox image = new PictureBox();
        public bool isClicked = false;
        public int protectiveInstalationAmount;
        public int reflectionInstalationAmount;
        public int turn = 0;

        public Ship(int health, Image image)
        {
            this.health = health;
            this.image.Image = image;
            this.image.BackColor = Color.Transparent;
            this.image.SizeMode = PictureBoxSizeMode.AutoSize;
        }

        public Ship() { }
        public double ProtectivePercent
        {
            get
            {
                return Math.Round((1 - Math.Pow(0.75, protectiveInstalationAmount)) * 100);
            }
        }

        public double ReflectionPercent
        {
            get
            {
                return Math.Round((1 - Math.Pow(0.7, reflectionInstalationAmount)) * 100);
            }
        }

        public void DefineBorderCells(Cell[,] field)
        {
            foreach (var position in positions)
            {
                for (int i = -1; i < 2; i++)
                    for (int j = -1; j < 2; j++)
                    {
                        var nextPosition = new Point(position.X + i, position.Y + j);
                        if (Game.InsideMap(nextPosition)
                            && (field[nextPosition.X, nextPosition.Y] == Cell.Empty
                            || field[nextPosition.X, nextPosition.Y] == Cell.NotActive))
                        {
                            field[nextPosition.X, nextPosition.Y] = Cell.NotActive;
                            borderCells.Add(nextPosition);
                        }
                    }
            }
        }

    }
}
