﻿using System;
using System.Collections.Generic;
using System.Drawing;

namespace Metal_Sea
{
    public static class Features
    {
        private static Random random = new Random();

        public static Point DoSplash(Point position, Player defender, Player attacker, int random, Game game)
        {
            if (random <= attacker.FuryPercent)
            {
                var possibleAdjacentCells = GetAdjacentCells(position, defender.field);
                var shootingPosition = possibleAdjacentCells[Features.random.Next(possibleAdjacentCells.Count)];
                game.Makemove(shootingPosition, defender, attacker, Features.random.Next(100));
                if (attacker == game.bot)
                    game.bot.UpdateInfo(shootingPosition, game.player.field);
                return shootingPosition;
            }
            return null;
        }

        public static bool UseScan(Point position, Player defender, Player attacker)
        {
            if (attacker.Money >= 300)
            {
                attacker.Money -= 300;
                for (int i = 0; i < 2; i++)
                    for (int j = 0; j < 2; j++)
                        if (Game.InsideMap(position.X + i, position.Y + j)
                            && defender.field[position.X + i, position.Y + j] == Cell.Occupied
                            || defender.field[position.X + i, position.Y + j] == Cell.MissedOnShip)
                            return true;
            }
            return false;
        }

        public static Point UseArrow(Point position, Size size, Player defender, Player attacker, Game game)
        {
            var stepX = size.Width < 0 ? -1 : 1;
            var stepY = size.Height < 0 ? -1 : 1;
            if (attacker.Money >= 150)
            {
                for (int i = 0; i <= Math.Abs(size.Width); i++)
                    for (int j = 0; j <= Math.Abs(size.Height); j++)
                    {
                        if (defender.field[position.X + i * stepX, position.Y + j * stepY] == Cell.Occupied ||
                    defender.field[position.X + i * stepX, position.Y + j * stepY] == Cell.MissedOnShip)
                        {
                            game.RegisterContact(new Point(position.X + i * stepX, position.Y + j * stepY), defender, attacker);
                            return new Point(position.X + i * stepX, position.Y + j * stepY);
                        }
                        if (defender.field[position.X + i * stepX, position.Y + j * stepY] == Cell.Blocked ||
                    defender.field[position.X + i * stepX, position.Y + j * stepY] == Cell.Wounded)
                            return null;
                    }
            }
            return null;
        }

        private static List<Point> GetAdjacentCells(Point position, Cell[,] field)
        {
            var result = new List<Point>();
            for (int i = -1; i < 2; i++)
                for (int j = -1; j < 2; j++)
                {
                    var nextPosition = new Point(position.X + i, position.Y + j);
                    if (Game.InsideMap(nextPosition)
                        && field[nextPosition.X, nextPosition.Y] != Cell.Wounded
                        && field[nextPosition.X, nextPosition.Y] != Cell.Blocked)
                    {
                        result.Add(nextPosition);
                    }
                }
            return result;
        }
    }
}
