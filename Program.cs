﻿using System.Drawing;
using System.Windows.Forms;

namespace Metal_Sea
{
    class Program
    {
        static void Main(string[] args)
        {
            Application.Run(new StartForm(new Game())
            {
                BackgroundImage = Image.FromFile(@"D:\Загрузки\ЯТП2\Metal Sea\images\back.png"),
                BackgroundImageLayout = ImageLayout.Stretch,
                StartPosition = FormStartPosition.CenterScreen,
                ClientSize = new Size(780, 780),
            });
        }
    }
}
