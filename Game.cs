﻿using System;
using System.Linq;

namespace Metal_Sea
{
    public class Game
    {
        public Human player = new Human();
        public Bot bot = new Bot(size);
        public Player move;
        public Random random = new Random();
        public event Action<Point, Player, Cell> modelChanged;
        private static int size = 10;
        public bool IsGameCompleted
        {
            get
            {
                return !(player.IsAlive && bot.IsAlive);
            }
        }

        public Player Winner
        {
            get
            {
                if (player.IsAlive && bot.IsAlive)
                    return null;
                return player.IsAlive ? (Player)player : bot;
            }
        }

        public Game()
        {
            move = player;
            for (int i = 0; i < 10; i++)
            {
                for (int j = 0; j < 10; j++)
                    if (bot.field[j, i] == Cell.Occupied)
                        Console.Write("O ");
                    else if (bot.field[j, i] == Cell.Empty)
                        Console.Write("  ");
                    else if (bot.field[j, i] == Cell.NotActive)
                        Console.Write("+ ");
                Console.WriteLine();
            }
            foreach (var ship in bot.ships)
            {
                Console.Write(ship.health + ": ");
                foreach (var position in ship.positions)
                    Console.Write(position + " ");
                Console.WriteLine();

            }
        }

        public void Makemove(Point position, Player defender, Player attacker, int random)
        {
            var isHit = false;
            var isCorrect = false;
            if (defender.field[position.X, position.Y] == Cell.Occupied ||
                defender.field[position.X, position.Y] == Cell.MissedOnShip)
            {
                isCorrect = true;
                var currentShip = defender.ships.First(x => x.positions.Contains(position));
                if (random <= currentShip.ReflectionPercent)
                {
                    var shootingPosition = DefinePositionToShoot(attacker.field);
                    Makemove(shootingPosition, attacker, defender, this.random.Next(100));
                    if (defender == bot)
                        bot.UpdateInfo(shootingPosition, player.field);
                }
                else if (random >= currentShip.ProtectivePercent)
                {
                    isHit = true;
                    RegisterContact(position, defender, attacker);
                    FinishMove(position, defender, attacker, random);
                }
                else
                {
                    defender.field[position.X, position.Y] = Cell.MissedOnShip;
                    FinishMove(position, defender, attacker, random);
                }
            }
            else if (defender.field[position.X, position.Y] == Cell.Unknown
                || defender.field[position.X, position.Y] == Cell.Empty
                || defender.field[position.X, position.Y] == Cell.NotActive)
            {
                isCorrect = true;
                defender.field[position.X, position.Y] = Cell.Unknown;
                FinishMove(position, defender, attacker, random);
            }
            move = isHit || !isCorrect ? attacker : defender;
        }

        private Point DefinePositionToShoot(Cell[,] opponentField)
        {
            var possibleShootingPositions = opponentField.FindAllIndexes(x => x == Cell.Empty || x == Cell.MissedOnShip
                                                                        || x == Cell.Unknown || x == Cell.Occupied);
            return possibleShootingPositions[random.Next(possibleShootingPositions.Count)];
        }

        private void FinishMove(Point position, Player defender, Player attacker, int random)
        {
            if (modelChanged != null)
                modelChanged(position, defender, defender.field[position.X, position.Y]);
            var shootingPosition = Features.DoSplash(position, defender, attacker, random, this);
            if (shootingPosition != null && modelChanged != null)
                modelChanged(shootingPosition, defender, defender.field[shootingPosition.X, shootingPosition.Y]);
        }
        public void Continue(Point position)
        {
            if (move == player && position != null)
            {
                player.Money += 50;
                Makemove(position, bot, player, random.Next(100));
            }
            else if (move == bot)
            {
                bot.Money += 50;
                position = bot.DefinePositionToShoot(player.field);
                Makemove(position, player, bot, random.Next(100));
                bot.TryBuySomething();
                bot.UpdateInfo(position, player.field);

            }
        }

        public void RegisterContact(Point position, Player defender, Player attacker)
        {
            var currentShip = defender.ships.First(x => x.positions.Contains(position));
            currentShip.health--;
            if (currentShip.health > 0)
                defender.field[position.X, position.Y] = Cell.Wounded;
            else
            {
                foreach (var e in currentShip.positions)
                {
                    defender.field[e.X, e.Y] = Cell.Dead;
                    attacker.Money += 50;
                }
                foreach (var e in currentShip.borderCells)
                    defender.field[e.X, e.Y] = Cell.Blocked;
            }
        }

        public static bool InsideMap(Point p) => InsideMap(p.X, p.Y);

        public static bool InsideMap(int x, int y)
        {
            return x >= 0 && x < size && y >= 0 && y < size;
        }

    }
}
