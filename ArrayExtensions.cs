﻿using System;
using System.Collections.Generic;

namespace Metal_Sea
{
    public static class ArrayExtensions
    {
        public static List<Point> FindAllIndexes<T>(this T[,] array, Func<T,bool> match)
        {
            var width = array.GetLength(0);
            var height = array.GetLength(1);
            var result = new List<Point>();
            for (int i = 0; i < width; i++)
                for (int j = 0; j < height; j++)
                    if (match(array[i, j]))
                        result.Add(new Point(i, j));
            return result;
        }
    }
}
