﻿using System;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;

namespace Metal_Sea
{
    public class Tests
    {
        private const string field1 = @"
OOOOEOOOEE
EEEEEEEEEE
OOOEOOEOOE
EEEEEEEEEE
OOEOEOEOEO
EEEEEEEEEE
EEEEEEEEEE
EEEEEEEEEE
EEEEEEEEEE
EEEEEEEEEE";
        private Game game = new Game();
        [TestCase(field1)]
        public void SimpleTest(string fieldBot)
        {
            var shootingPosition = new Point(0, 0);
            var game = new Game();
            FillField(fieldBot, game.bot);
            var currentShip = game.bot.ships.First(x => x.positions.Contains(shootingPosition));
            game.RegisterContact(new Point(0, 0), game.bot, game.player);
            Assert.AreEqual(Cell.Wounded, game.bot.field[0, 0]);
            Assert.AreEqual(3, currentShip.health);
        }
        [TestCase(field1)]
        public void SimpleTest2(string field)
        {
            var expected = new HashSet<Point>()
            {
                new Point(4,0),
                new Point(0,1),
                new Point(1,1),
                new Point(2,1),
                new Point(3,1),
                new Point(4,1)
            };
            var game = new Game();
            FillField(field, game.bot);
            var shipPosition = new Point(0, 0);
            var currentShip = game.bot.ships.First(x => x.positions.Contains(shipPosition));
            currentShip.DefineBorderCells(game.bot.field);
            Assert.That(currentShip.borderCells,Is.EquivalentTo(expected));
        }

        [TestCase(field1)]
        public void SimpleTest3(string fieldBot)
        {
            var game = new Game();
            game.player.Money += 150;
            FillField(fieldBot, game.bot);
            var scanPosition = new Point(3, 0);
            var result = Features.UseScan(scanPosition, game.bot, game.player);
            Assert.IsTrue(result);
        }

        [TestCase(field1)]
        public void SimpleTest4(string fieldBot)
        {
            var game = new Game();
            game.player.Money += 150;
            FillField(fieldBot, game.bot);
            var startPosition = new Point(0, 6);
            var result=Features.UseArrow(startPosition, new System.Drawing.Size(0, -3), game.bot, game.player, game);
            Assert.AreEqual(new Point(0, 4), result);
            Assert.AreEqual(Cell.Wounded,game.bot.field[result.X,result.Y]);
        }
        [TestCase(field1)]
        public void SimpleTest5(string fieldPlayer)
        {
            var game = new Game();
            FillField(fieldPlayer, game.player);
            var shootingPosition = new Point(2,2);
            game.RegisterContact(shootingPosition, game.player, game.bot);
            game.bot.UpdateInfo(shootingPosition, game.player.field);
            var result = game.bot.infoList[0];
            Assert.That(result.successShoots,Is.EquivalentTo(new Point[] { new Point(2, 2) }));
            var expectedRecommendedMoves = new Point[] { new Point(1, 2), new Point(2, 1), new Point(2, 3), new Point(3, 2) };
            Assert.That(result.recommendedMoves, Is.EquivalentTo(expectedRecommendedMoves));
        }

        public void FillField(string map, Player player, string separator = "\r\n")
        {
            var rows = map.Split(new[] { separator }, StringSplitOptions.RemoveEmptyEntries);
            if (rows.Select(z => z.Length).Distinct().Count() != 1)
                throw new Exception($"Wrong test map '{map}'");
            player.field = new Cell[rows[0].Length, rows.Length];
            for (var x = 0; x < rows[0].Length; x++)
                for (var y = 0; y < rows.Length; y++)
                {
                    player.field[x, y] = CreateCreatureBySymbol(rows[y][x]);
                    if (player.field[x, y] == Cell.Occupied)
                        AddShipPosition(new Point(x, y),player);
                }
        }

        private static Cell CreateCreatureBySymbol(char c)
        {
            if (c == 'E')
                return Cell.Empty;
            else if (c == 'O')
                return Cell.Occupied;
            else
                throw new Exception($"wrong character for Cell {c}");
        }

        private void AddShipPosition(Point position, Player player)
        {
            var cellWithShip = Player.possibleAdjacentCells
                .Select(e => new Point(position.X + e.X, position.Y + e.Y))
                .FirstOrDefault(e => Game.InsideMap(e) && player.field[e.X, e.Y] == Cell.Occupied);
            if (cellWithShip != null)
            {
                var currentShip = player.ships.First(x => x.positions.Contains(cellWithShip));
                currentShip.positions.Add(position);
                currentShip.health++;
            }
            else
            {
                var ship = new Ship();
                ship.health++;
                ship.positions.Add(position);
                player.ships.Add(ship);
            }
        }
    }
}
