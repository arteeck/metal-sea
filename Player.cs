﻿using System;
using System.Collections.Generic;
using System.Drawing;

namespace Metal_Sea
{
    public class Player
    {
        public Cell[,] field = new Cell[10, 10];
        public List<Ship> ships = new List<Ship>
        {
            new Ship(4,Image.FromFile(@"D:\Загрузки\ЯТП2\Metal Sea\images\blue4.png")),
            new Ship(3,Image.FromFile(@"D:\Загрузки\ЯТП2\Metal Sea\images\orange3.png")),
            new Ship(3,Image.FromFile(@"D:\Загрузки\ЯТП2\Metal Sea\images\orange33.png")),
            new Ship(2,Image.FromFile(@"D:\Загрузки\ЯТП2\Metal Sea\images\green2.png")),
            new Ship(2,Image.FromFile(@"D:\Загрузки\ЯТП2\Metal Sea\images\red2.png")),
            new Ship(2,Image.FromFile(@"D:\Загрузки\ЯТП2\Metal Sea\images\yellow2.png")),
            new Ship(1,Image.FromFile(@"D:\Загрузки\ЯТП2\Metal Sea\images\blue1.png")),
            new Ship(1,Image.FromFile(@"D:\Загрузки\ЯТП2\Metal Sea\images\green1.png")),
            new Ship(1,Image.FromFile(@"D:\Загрузки\ЯТП2\Metal Sea\images\yellow1.png")),
            new Ship(1,Image.FromFile(@"D:\Загрузки\ЯТП2\Metal Sea\images\red1.png"))
        };
        public static readonly List<Point> possibleAdjacentCells = new List<Point>
            { new Point(0, -1), new Point(0, 1), new Point(-1, 0), new Point(1, 0) };

        private int money;
        public int Money
        {
            get => money;
            set
            {
                if (value < 0)
                    throw new ArgumentException("Money must be non-negative");
                money = value;
            }
        }
        public int FuryAmount=1;
        public double FuryPercent
        {
            get
            {
                return Math.Round((1 - Math.Pow(0.9, FuryAmount)) * 100);
            }
        }

        public bool IsAlive
        {
            get
            {
                foreach (var ship in ships)
                    if (ship.health > 0)
                        return true;
                return false;
            }
        }

        public void BuyFury()
        {
            if (Money >= 500)
            {
                Money -= 500;
                FuryAmount++;
            }
        }

        public void BuyProtectiveInstallation(Ship ship)
        {
            if (Money >= 500)
            {
                ship.protectiveInstalationAmount++;
                Money -= 500;
            }
        }

        public void BuyReflectiveInstallation(Ship ship)
        {
            if (Money >= 1000)
            {
                ship.reflectionInstalationAmount++;
                Money -= 1000;
            }
        }
    }
}
