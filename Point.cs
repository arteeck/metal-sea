﻿namespace Metal_Sea
{

    public class Point
    {

        public int X { get; set; }
        public int Y { get; set; }
        public Point(int x, int y)
        {
            X = x;
            Y = y;
        }
        public override bool Equals(object obj)
        {
            var point = obj as Point;
            return X == point.X && Y == point.Y;
        }
        public override int GetHashCode()
        {
            unchecked
            {
                return X * 1023 + Y;
            }
        }
        public override string ToString()
        {
            return string.Format("{0},{1}", X, Y);
        }
    }
}
