﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Metal_Sea
{
    public class Bot : Player
    {
        public class Info
        {
            public List<Point> recommendedMoves = new List<Point>();
            public List<Point> successShoots = new List<Point>();
        }
        public List<Info> infoList = new List<Info>();
        private Random random = new Random();
        private string[] products = new string[] { "Fury", "Protect", "Reflect" };

        public Bot(int size)
        {
            field = RandomField.Make(size, ships);
        }

        public void UpdateInfo(Point position, Cell[,] opponentField)
        {
            var info = infoList.FirstOrDefault(e => e.recommendedMoves.Contains(position));
            if (opponentField[position.X, position.Y] == Cell.Wounded)
            {
                if (info == null)
                {
                    info = new Info();
                    infoList.Add(info);
                }
                info.recommendedMoves.Clear();
                info.successShoots.Add(position);
                DetermineRecommendedMoves(opponentField, info);
            }
            else if (opponentField[position.X, position.Y] == Cell.Dead)
            {
                if (info != null)
                    infoList.Remove(info);
            }
            else if (info != null)
                info.recommendedMoves.Remove(position);
        }

        public Point DefinePositionToShoot(Cell[,] opponentField)
        {
            if (infoList.Count == 0)
            {
                var possibleShootingPositions = opponentField.FindAllIndexes(x => x == Cell.Empty || x == Cell.MissedOnShip
                                                                            || x == Cell.Unknown || x == Cell.Occupied);
                return possibleShootingPositions[random.Next(possibleShootingPositions.Count)];
            }
            var info = infoList[random.Next(infoList.Count)];
            return info.recommendedMoves[random.Next(info.recommendedMoves.Count)];
        }

        private void DetermineRecommendedMoves(Cell[,] opponentField, Info info)
        {
            if (info.successShoots.Count == 0)
                return;
            if (info.successShoots.Count == 1)
                info.recommendedMoves
                    .AddRange(
                    possibleAdjacentCells
                    .Select(e => new Point(info.successShoots[0].X + e.X, info.successShoots[0].Y + e.Y))
                    .Where(e => Game.InsideMap(e) && opponentField[e.X, e.Y] != Cell.Blocked));
            else if (info.successShoots[0].X != info.successShoots[1].X)
                DetermineRecommendedMoves(e => e.X, opponentField, info);
            else
                DetermineRecommendedMoves(e => e.Y, opponentField, info);

        }

        private void DetermineRecommendedMoves(Func<Point, int> selector, Cell[,] opponentField, Info info)
        {
            var minValue = info.successShoots.Min(selector);
            var maxValue = info.successShoots.Max(selector);
            var minAndMax = new Point[] { info.successShoots.Find(x => selector(x) == minValue), info.successShoots.Find(x => selector(x) == maxValue) };
            info.recommendedMoves
                    .AddRange(
                    possibleAdjacentCells
                    .Where(e => selector(e) != 0)
                    .Zip(minAndMax, (a, b) => new Point(a.X + b.X, a.Y + b.Y))
                    .Where(e => Game.InsideMap(e) && opponentField[e.X, e.Y] != Cell.Blocked));
        }

        public void TryBuySomething()
        {
            var product = products[random.Next(products.Length)];
            if (product == "Fury")
                BuyFury();
            else if (product == "Protect")
                BuyProtectiveInstallation(GetRandomShip());
            else
                BuyReflectiveInstallation(GetRandomShip());
        }

        private Ship GetRandomShip()
        {
            var aliveShips = ships.Where(x => x.health > 0).ToList();
            return aliveShips[random.Next(aliveShips.Count)];
        }

        public override string ToString()
        {
            return "Компьютер";
        }
    }
}
