﻿using System;

namespace Metal_Sea
{
    public static class SetUpBot
    {
        static bool FindEmpty(Cell point)
        {
            return point == Cell.Empty;
        }

        public static Cell[,] SetUp()
        {
            var field = new Cell[10, 10];
            for (int i = 0; i < 10; i++)
            {
                for (int j = 0; j < 10; j++)
                {
                    field[i, j] = Cell.Empty;
                }
            }
            var rand = new Random();
            var x = rand.Next(0, 10);
            var y = rand.Next(0, 10);
            field[y, x] = Cell.Occupied;
            var z = rand.Next(0, 2);
            var t = rand.Next(0, 2);
            if (z == 0 && t == 0)
            {
                for (int i = 1; i < 4; i++)
                {
                    if (x == 9)
                    {
                        x = x - i;
                        field[y, x] = Cell.Occupied;
                        x = 9;
                    }
                    else
                    {
                        x++;
                        field[y, x] = Cell.Occupied;
                    }
                }
            }
            else if (z == 0 && t == 1)
            {
                for (int i = 1; i < 4; i++)
                {
                    if (x == 0)
                    {
                        x = x + i;
                        field[y, x] = Cell.Occupied;
                        x = 0;
                    }
                    else
                    {
                        x--;
                        field[y, x] = Cell.Occupied;
                    }
                }
            }
            else if (z == 1 && t == 0)
            {
                for (int i = 1; i < 4; i++)
                {
                    if (y == 9)
                    {
                        y = y - i;
                        field[y, x] = Cell.Occupied;
                        y = 9;
                    }
                    else
                    {
                        y++;
                        field[y, x] = Cell.Occupied;
                    }
                }
            }
            else
            {
                for (int i = 1; i < 4; i++)
                {
                    if (y == 0)
                    {
                        y = y + i;
                        field[y, x] = Cell.Occupied;
                        y = 0;
                    }
                    else
                    {
                        y--;
                        field[y, x] = Cell.Occupied;
                    }
                }
            }
            CircleTheShip(field);
            var q = 0;
            while (q < 2)
            {
                ArrangeTheShips3(field, rand);
                q++;
            }
            q = 0;
            while (q < 3)
            {
                ArrangeTheShips2(field, rand);
                q++;
            }
            q = 0;
            while (q < 4)
            {
                var newList = ArrayExtensions.FindAllIndexes(field, FindEmpty);

                var number = rand.Next(0, newList.Count);
                x = newList[number].Y;
                y = newList[number].X;
                field[y, x] = Cell.Occupied;
                CircleTheShip(field);
                q++;
            }
            return field;
        }

        public static void ArrangeTheShips3(this Cell[,] field, Random rand)
        {
            var newList = ArrayExtensions.FindAllIndexes(field, FindEmpty);

            var number = rand.Next(0, newList.Count);
            var x = newList[number].Y;
            var y = newList[number].X;
            var z = rand.Next(0, 2);
            if (z == 0)
            {
                if (x == 9)
                {
                    if (field[y, x - 1] == Cell.Empty && field[y, x - 2] == Cell.Empty)
                    {
                        field[y, x] = Cell.Occupied;
                        field[y, x - 1] = Cell.Occupied;
                        field[y, x - 2] = Cell.Occupied;
                    }
                    else
                        ArrangeTheShips3(field, rand);
                }
                else if (x == 8)
                {
                    if (field[y, x - 1] == Cell.Empty && field[y, x - 2] == Cell.Empty)
                    {
                        field[y, x] = Cell.Occupied;
                        field[y, x - 1] = Cell.Occupied;
                        field[y, x - 2] = Cell.Occupied;
                    }
                    else if (field[y, x + 1] == Cell.Empty && field[y, x - 1] == Cell.Empty)
                    {
                        field[y, x] = Cell.Occupied;
                        field[y, x + 1] = Cell.Occupied;
                        field[y, x - 1] = Cell.Occupied;
                    }
                    else
                        ArrangeTheShips3(field, rand);
                }
                else if (x == 0)
                {
                    if (field[y, x + 1] == Cell.Empty && field[y, x + 2] == Cell.Empty)
                    {
                        field[y, x] = Cell.Occupied;
                        field[y, x + 1] = Cell.Occupied;
                        field[y, x + 2] = Cell.Occupied;
                    }
                    else
                        ArrangeTheShips3(field, rand);
                }
                else if (x == 1)
                {
                    if (field[y, x + 1] == Cell.Empty && field[y, x + 2] == Cell.Empty)
                    {
                        field[y, x] = Cell.Occupied;
                        field[y, x + 1] = Cell.Occupied;
                        field[y, x + 2] = Cell.Occupied;
                    }
                    else if (field[y, x - 1] == Cell.Empty && field[y, x + 1] == Cell.Empty)
                    {
                        field[y, x] = Cell.Occupied;
                        field[y, x + 1] = Cell.Occupied;
                        field[y, x - 1] = Cell.Occupied;
                    }
                    else
                        ArrangeTheShips3(field, rand);
                }
                else
                {
                    if (field[y, x + 1] == Cell.Empty && field[y, x + 2] == Cell.Empty)
                    {
                        field[y, x] = Cell.Occupied;
                        field[y, x + 1] = Cell.Occupied;
                        field[y, x + 2] = Cell.Occupied;
                    }
                    else if (field[y, x - 1] == Cell.Empty && field[y, x + 1] == Cell.Empty)
                    {
                        field[y, x] = Cell.Occupied;
                        field[y, x + 1] = Cell.Occupied;
                        field[y, x - 1] = Cell.Occupied;
                    }
                    else if (field[y, x - 1] == Cell.Empty && field[y, x - 2] == Cell.Empty)
                    {
                        field[y, x] = Cell.Occupied;
                        field[y, x - 1] = Cell.Occupied;
                        field[y, x - 2] = Cell.Occupied;
                    }
                    else
                        ArrangeTheShips3(field, rand);
                }
            }
            else
            {
                if (y == 9)
                {
                    if (field[y - 1, x] == Cell.Empty && field[y - 2, x] == Cell.Empty)
                    {
                        field[y, x] = Cell.Occupied;
                        field[y - 1, x] = Cell.Occupied;
                        field[y - 2, x] = Cell.Occupied;
                    }
                    else
                        ArrangeTheShips3(field, rand);
                }
                else if (y == 8)
                {
                    if (field[y - 1, x] == Cell.Empty && field[y - 2, x] == Cell.Empty)
                    {
                        field[y, x] = Cell.Occupied;
                        field[y - 1, x] = Cell.Occupied;
                        field[y - 2, x] = Cell.Occupied;
                    }
                    else if (field[y + 1, x] == Cell.Empty && field[y - 1, x] == Cell.Empty)
                    {
                        field[y, x] = Cell.Occupied;
                        field[y + 1, x] = Cell.Occupied;
                        field[y - 1, x] = Cell.Occupied;
                    }
                    else
                        ArrangeTheShips3(field, rand);
                }
                else if (y == 0)
                {
                    if (field[y + 1, x] == Cell.Empty && field[y + 2, x] == Cell.Empty)
                    {
                        field[y, x] = Cell.Occupied;
                        field[y + 1, x] = Cell.Occupied;
                        field[y + 2, x] = Cell.Occupied;
                    }
                    else
                        ArrangeTheShips3(field, rand);
                }
                else if (y == 1)
                {
                    if (field[y + 1, x] == Cell.Empty && field[y + 2, x] == Cell.Empty)
                    {
                        field[y, x] = Cell.Occupied;
                        field[y + 1, x] = Cell.Occupied;
                        field[y + 2, x] = Cell.Occupied;
                    }
                    else if (field[y - 1, x] == Cell.Empty && field[y + 1, x] == Cell.Empty)
                    {
                        field[y, x] = Cell.Occupied;
                        field[y - 1, x] = Cell.Occupied;
                        field[y + 1, x] = Cell.Occupied;
                    }
                    else
                        ArrangeTheShips3(field, rand);
                }
                else
                {
                    if (field[y + 1, x] == Cell.Empty && field[y + 2, x] == Cell.Empty)
                    {
                        field[y, x] = Cell.Occupied;
                        field[y + 1, x] = Cell.Occupied;
                        field[y + 2, x] = Cell.Occupied;
                    }
                    else if (field[y - 1, x] == Cell.Empty && field[y + 1, x] == Cell.Empty)
                    {
                        field[y, x] = Cell.Occupied;
                        field[y - 1, x] = Cell.Occupied;
                        field[y + 1, x] = Cell.Occupied;
                    }
                    else if (field[y - 1, x] == Cell.Empty && field[y - 2, x] == Cell.Empty)
                    {
                        field[y, x] = Cell.Occupied;
                        field[y - 1, x] = Cell.Occupied;
                        field[y - 2, x] = Cell.Occupied;
                    }
                    else
                        ArrangeTheShips3(field, rand);
                }
            }
            CircleTheShip(field);
        }

        public static void ArrangeTheShips2(this Cell[,] field, Random rand)
        {
            var newList = ArrayExtensions.FindAllIndexes(field, FindEmpty);

            var number = rand.Next(0, newList.Count);
            var x = newList[number].Y;
            var y = newList[number].X;
            var z = rand.Next(0, 2);
            if (z == 0)
            {
                if (x == 9)
                {
                    if (field[y, x - 1] == Cell.Empty)
                    {
                        field[y, x] = Cell.Occupied;
                        field[y, x - 1] = Cell.Occupied;
                    }
                    else
                        ArrangeTheShips2(field, rand);
                }
                else if (x == 0)
                {
                    if (field[y, x + 1] == Cell.Empty)
                    {
                        field[y, x] = Cell.Occupied;
                        field[y, x + 1] = Cell.Occupied;
                    }
                    else
                        ArrangeTheShips2(field, rand);
                }
                else
                {
                    if (field[y, x + 1] == Cell.Empty)
                    {
                        field[y, x] = Cell.Occupied;
                        field[y, x + 1] = Cell.Occupied;
                    }
                    else if (field[y, x - 1] == Cell.Empty)
                    {
                        field[y, x] = Cell.Occupied;
                        field[y, x - 1] = Cell.Occupied;
                    }
                    else
                        ArrangeTheShips2(field, rand);
                }
            }
            else
            {
                if (y == 9)
                {
                    if (field[y - 1, x] == Cell.Empty)
                    {
                        field[y, x] = Cell.Occupied;
                        field[y - 1, x] = Cell.Occupied;
                    }
                    else
                        ArrangeTheShips2(field, rand);
                }
                else if (y == 0)
                {
                    if (field[y + 1, x] == Cell.Empty)
                    {
                        field[y, x] = Cell.Occupied;
                        field[y + 1, x] = Cell.Occupied;
                    }
                    else
                        ArrangeTheShips2(field, rand);
                }
                else
                {
                    if (field[y + 1, x] == Cell.Empty)
                    {
                        field[y, x] = Cell.Occupied;
                        field[y + 1, x] = Cell.Occupied;
                    }
                    else if (field[y - 1, x] == Cell.Empty)
                    {
                        field[y, x] = Cell.Occupied;
                        field[y - 1, x] = Cell.Occupied;
                    }
                    else
                        ArrangeTheShips2(field, rand);
                }
            }
            CircleTheShip(field);
        }

        public static void CircleTheShip(this Cell[,] field)
        {
            for (int i = 0; i < 10; i++)
                for (int j = 0; j < 10; j++)
                {
                    if (i != 0 && i != 9 && j != 0 && j != 9)
                        if (field[i, j] == Cell.Empty && (field[i + 1, j] == Cell.Occupied || field[i - 1, j] == Cell.Occupied || field[i + 1, j + 1] == Cell.Occupied || field[i + 1, j - 1] == Cell.Occupied || field[i - 1, j + 1] == Cell.Occupied || field[i - 1, j - 1] == Cell.Occupied || field[i, j - 1] == Cell.Occupied || field[i, j + 1] == Cell.Occupied))

                            field[i, j] = Cell.NotActive;
                    if (i == 0 && j != 9 && j != 0)
                        if (field[i, j] == Cell.Empty && (field[i + 1, j] == Cell.Occupied || field[i + 1, j + 1] == Cell.Occupied || field[i + 1, j - 1] == Cell.Occupied || field[i, j - 1] == Cell.Occupied || field[i, j + 1] == Cell.Occupied))
                            field[i, j] = Cell.NotActive;
                    if (j == 0 && i != 9 && i != 0)
                        if (field[i, j] == Cell.Empty && (field[i + 1, j] == Cell.Occupied || field[i - 1, j] == Cell.Occupied || field[i + 1, j + 1] == Cell.Occupied || field[i - 1, j + 1] == Cell.Occupied || field[i, j + 1] == Cell.Occupied))
                            field[i, j] = Cell.NotActive;
                    if (i == 9 && j != 9 && j != 0)
                        if (field[i, j] == Cell.Empty && (field[i - 1, j] == Cell.Occupied || field[i - 1, j + 1] == Cell.Occupied || field[i - 1, j - 1] == Cell.Occupied || field[i, j - 1] == Cell.Occupied || field[i, j + 1] == Cell.Occupied))
                            field[i, j] = Cell.NotActive;
                    if (j == 9 && i != 9 && i != 0)
                        if (field[i, j] == Cell.Empty && (field[i + 1, j] == Cell.Occupied || field[i - 1, j] == Cell.Occupied || field[i + 1, j - 1] == Cell.Occupied || field[i - 1, j - 1] == Cell.Occupied || field[i, j - 1] == Cell.Occupied))
                            field[i, j] = Cell.NotActive;
                    if (i == 0 && j == 0)
                        if (field[i, j] == Cell.Empty && (field[i + 1, j] == Cell.Occupied || field[i + 1, j + 1] == Cell.Occupied || field[i, j + 1] == Cell.Occupied))
                            field[i, j] = Cell.NotActive;
                    if (i == 0 && j == 9)
                        if (field[i, j] == Cell.Empty && (field[i + 1, j] == Cell.Occupied || field[i + 1, j - 1] == Cell.Occupied || field[i, j - 1] == Cell.Occupied))
                            field[i, j] = Cell.NotActive;
                    if (i == 9 && j == 0)
                        if (field[i, j] == Cell.Empty && (field[i - 1, j] == Cell.Occupied || field[i - 1, j + 1] == Cell.Occupied || field[i, j + 1] == Cell.Occupied))
                            field[i, j] = Cell.NotActive;
                    if (i == 9 && j == 9)
                        if (field[i, j] == Cell.Empty && (field[i - 1, j] == Cell.Occupied || field[i - 1, j - 1] == Cell.Occupied || field[i, j - 1] == Cell.Occupied))
                            field[i, j] = Cell.NotActive;
                }
        }
    }
}