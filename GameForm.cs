﻿using System;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace Metal_Sea
{
    public class GameForm : Form
    {
        private System.Windows.Forms.Timer timer = new System.Windows.Forms.Timer()
        {
            Interval = 300
        };
        private Game game;
        private Image explosion = Image.FromFile(@"D:\Загрузки\ЯТП2\Metal Sea\images\explosion.png");
        private Image miss = Image.FromFile(@"D:\Загрузки\ЯТП2\Metal Sea\images\miss1.png");
        private Image blocked = Image.FromFile(@"D:\Загрузки\ЯТП2\Metal Sea\images\blocked.png");
        private Image blocked1 = Image.FromFile(@"D:\Загрузки\ЯТП2\Metal Sea\images\blocked1.png");
        private bool usingScan = false;
        private Button useScan = new Button()
        {
            Location = new System.Drawing.Point(230, 700),
            AutoSizeMode = AutoSizeMode.GrowAndShrink,
            Text = "Use Scan",
            Cursor = Cursors.Hand,
            BackColor = Color.Aquamarine,
        };
        private ToolTip infoForScan = new ToolTip();
        private Button buyFury = new Button()
        {
            Location = new System.Drawing.Point(130, 700),
            AutoSizeMode = AutoSizeMode.GrowAndShrink,
            Text = "Buy Fury",
            Cursor = Cursors.Hand,
            BackColor = Color.Aquamarine,
        };
        private ToolTip infoForFury = new ToolTip();
        private Point shootingPosition;

        public GameForm(Game game)
        {
            SetTools();
            Controls.Add(buyFury);
            Controls.Add(useScan);
            this.game = game;
            DoubleBuffered = true;         
            timer.Tick += (sender, args) =>
            {
                if (usingScan && shootingPosition != null)
                {
                    usingScan = false;
                    PerformScan();
                    Cursor = Cursors.Default;
                }
                else
                    game.Continue(shootingPosition);
                shootingPosition = null;
            };
            timer.Tick += CheckGameFinished;
            MouseClick += (sender, args) =>
            {
                if (args.X >= 420 && args.X < 720 && args.Y >= 240 && args.Y < 540)
                    shootingPosition = new Point((args.X - 420) / 30, (args.Y - 240) / 30);
                else
                {
                    usingScan = false;
                    Cursor = Cursors.Default;
                }
            };
            game.modelChanged += Update;
            Paint += DrawState;
            PrepareShipsToBattle();
            timer.Start();
        }

        private void PerformScan()
        {
            var result = Features.UseScan(shootingPosition, game.bot, game.player);
            shootingPosition = null;
            if (result)
                MessageBox.Show("Найден корабль");
            else
                MessageBox.Show("Пусто");
        }

        private void Update(Point position,Player player,Cell cell)
        {
            var topLeft = player == game.player ? new Point(61, 241) : new Point(421, 241);
            if (cell == Cell.Unknown || cell == Cell.MissedOnShip)
                DrawMissedShot(position, topLeft);
            else if (cell == Cell.Wounded)
                DrawWoundedShip(player, position, topLeft);
            else if (cell == Cell.Dead)
                DrawDeadShip(player, position, topLeft);
            Invalidate();
        }

        private void SetTools()
        {
            infoForFury.SetToolTip(buyFury, @"Цена: 500
С вероятностью в 10% совершит прорубающий выстрел
Сочетается по закону убывающей полезности");
            buyFury.Click += (sender, args) =>
            {
                if (game.player.Money < 500)
                    MessageBox.Show("Недостаточно средств");
                game.player.BuyFury();
                Invalidate();
            };
            infoForScan.SetToolTip(useScan, @"Цена: 300
Нажмите на левую верхнюю клетку квадрата 2х2,
который хотите проверить");
            useScan.Click += (sender, args) =>
            {
                if (game.player.Money >= 300)
                {
                    usingScan = true;
                    Cursor = Cursors.Hand;
                }
                else
                    MessageBox.Show("Недостаточно средств");
            };
        }

        private void CheckGameFinished(object sender, EventArgs e)
        {
            if (game.IsGameCompleted)
            {
                timer.Stop();
                MessageBox.Show(game.Winner.ToString() + " победил!");
            }
        }

        private void PrepareShipsToBattle()
        {
            foreach (var ship in game.player.ships)
            {
                ship.image.Left -= 180;
                ship.image.MouseDown += (sender, args) =>
                {
                    ship.isClicked = false;
                };
                ship.image.ContextMenuStrip = AddShipFunctional(ship);
                Controls.Add(ship.image);
                DefinePositionsInModel(ship);
                ship.DefineBorderCells(game.player.field);
            }
            foreach (var ship in game.bot.ships)
                DefinePositionsInForm(ship, new Point(420, 240));
        }
        private void DrawMissedShot(Point position, Point topLeft)
        {
            var currentPositon = new Point(position.X * 30 + topLeft.X, position.Y * 30 + topLeft.Y);
            Paint += (sender, args) =>
            {
                args.Graphics.DrawImage(miss, currentPositon.X, currentPositon.Y);
            };
        }
        private void DrawWoundedShip(Player player,Point position,Point topLeft)
        {
            var currentShip = player.ships.First(x => x.positions.Contains(position));

            var currentPositon = new Point(position.X * 30 + topLeft.X, position.Y * 30 + topLeft.Y);
            if (player == game.player)
            {
                currentShip.image.Paint += (sender, args) =>
                {
                    args.Graphics.DrawImage(explosion, currentPositon.X - currentShip.image.Left, currentPositon.Y - currentShip.image.Top);
                };
            }
            else
                Paint += (sender, args) =>
                {
                    args.Graphics.DrawImage(explosion, currentPositon.X, currentPositon.Y);
                };
        }
        private void DrawDeadShip(Player player, Point position, Point topLeft)
        {
            var currentShip = player.ships.First(x => x.positions.Contains(position));
            currentShip.image.Enabled = false;
            Controls.Add(currentShip.image);
            currentShip.image.Paint += (sender, args) =>
            {
                foreach (var e in currentShip.positions)
                {
                    var currentPositon = new Point(e.X * 30 + topLeft.X, e.Y * 30 + topLeft.Y);
                    args.Graphics.DrawImage(explosion, currentPositon.X - currentShip.image.Left, currentPositon.Y - currentShip.image.Top);
                }
            };
            foreach (var e in currentShip.positions)
            {
                var currentPositon = new Point(e.X * 30 + topLeft.X, e.Y * 30 + topLeft.Y);
                Paint += (sender, args) =>
                {
                    args.Graphics.FillRectangle(Brushes.Red, currentPositon.X, currentPositon.Y, 29, 29);
                };
            }
            foreach (var e in currentShip.borderCells)
            {
                var currentPositon = new Point(e.X * 30 + topLeft.X, e.Y * 30 + topLeft.Y);
                Paint += (sender, args) =>
                {
                    args.Graphics.DrawImage(blocked, currentPositon.X, currentPositon.Y);
                    args.Graphics.DrawImage(blocked1, currentPositon.X + 5, currentPositon.Y + 5);
                };
            }
        }
        private void DrawState(object sender, PaintEventArgs e)
        {
            var g = e.Graphics;
            g.DrawString(game.player.Money.ToString(), new Font("Arial", 32), Brushes.Yellow, new PointF(0, 0));
            g.FillRectangle(new SolidBrush(Color.FromArgb(50, Color.Indigo)), 60, 240, 300, 300);
            g.FillRectangle(new SolidBrush(Color.FromArgb(50, Color.Indigo)), 420, 240, 300, 300);
            for (int i = 0; i < 11; i++)
            {
                g.DrawLine(new Pen(Color.FromArgb(150, Color.White), 1), new PointF(60, 240 + i * 30), new PointF(360, 240 + i * 30));
                g.DrawLine(new Pen(Color.FromArgb(150, Color.White), 1), new PointF(60 + i * 30, 240), new PointF(60 + i * 30, 540));
                g.DrawLine(new Pen(Color.FromArgb(150, Color.White), 1), new PointF(420, 240 + i * 30), new PointF(720, 240 + i * 30));
                g.DrawLine(new Pen(Color.FromArgb(150, Color.White), 1), new PointF(420 + i * 30, 240), new PointF(420 + i * 30, 540));
            }
        }
        private void DefinePositionsInModel(Ship ship)
        {
            for (int i = ship.image.Left - 60; i < ship.image.Right - 60; i += 30)
                for (int j = ship.image.Top - 240; j < ship.image.Bottom - 240; j += 30)
                {
                    ship.positions.Add(new Point(i / 30, j / 30));
                    game.player.field[i / 30, j / 30] = Cell.Occupied;
                }
        }
        
        private void DefinePositionsInForm(Ship ship,Point fieldLocation)
        {
            var min = ship.positions.Min(e => e.X + e.Y);
            var topLeft = ship.positions.First(e => e.X + e.Y == min);
            ship.image.Location = new System.Drawing.Point(topLeft.X * 30 + fieldLocation.X, topLeft.Y * 30 + fieldLocation.Y);
            if (ship.positions.First().Y != ship.positions.Last().Y)
            {
                ship.image.Image.RotateFlip(RotateFlipType.Rotate90FlipX);
                ship.image.Size = ship.image.Image.Size;
            }
        }

        private ContextMenuStrip AddShipFunctional(Ship ship)
        {
            var menu = new ContextMenuStrip();
            var buyProtect = new ToolStripButton();
               buyProtect.Text = "Защитная установка";
            buyProtect.Click += (sender, args) =>
            {
                if (game.player.Money < 500)
                    MessageBox.Show("Недостаточно средств");
                game.player.BuyProtectiveInstallation(ship);
                Invalidate();
            };
            buyProtect.ToolTipText = @"Цена: 500
С вероятностью в 25% защитит корабль от попадания
Сочетается по закону убывающей полезности";
            menu.Items.Add(buyProtect);
            var buyReflect = new ToolStripButton();
               buyReflect.Text = "Рефлективная установка";
            buyReflect.Click += (sender, args) =>
            {
                if (game.player.Money < 1000)
                    MessageBox.Show("Недостаточно средств");
                game.player.BuyReflectiveInstallation(ship);
                Invalidate();
            };
            buyReflect.ToolTipText = @"Цена: 1000
С вероятностью в 30% корабль отразит выстрел в обратную сторону
Сочетается по закону убывающей полезности";
            menu.Items.Add(buyReflect);
            return menu;
        }
    }
}
