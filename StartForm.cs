﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace Metal_Sea
{
    public class StartForm : Form
    {
        private Game game;
        private Button playGame = new Button()
        {
            Location = new System.Drawing.Point(630, 360),
            AutoSizeMode = AutoSizeMode.GrowAndShrink,
            Image = Image.FromFile(@"D:\Загрузки\ЯТП2\Metal Sea\images\start.png"),
            Enabled = false,
            Cursor = Cursors.Hand,
            BackColor = Color.Black
        };
        private Point startClick = new Point(-1, -1);
        private System.Drawing.Point initialPosition = new System.Drawing.Point(-1, -1);
        private List<Point> startPositions = new List<Point>
        {
            new Point(240, 30), new Point(420, 30),new Point(240, 90), new Point(360, 90), new Point(450, 90),
            new Point(240, 150), new Point(330, 150), new Point(390, 150), new Point(450, 150), new Point(510, 150)
        };

        public StartForm(Game game)
        {
            this.game = game;
            playGame.Click += PlayGame;
            Controls.Add(playGame);
            DoubleBuffered = true;           
            Paint += DrawField;           
            for (int i = 0; i < 10; i++)
            {
                var currentShip = game.player.ships[i];
                currentShip.image.Location = new System.Drawing.Point(startPositions[i].X, startPositions[i].Y);
                Controls.Add(game.player.ships[i].image);
                SetShipMouseDown(currentShip);
                SetShipMouseUp(currentShip);
                SetShipMouseMove(currentShip);
            }
        }

        private void SetShipMouseDown(Ship ship)
        {
            ship.image.MouseDown += (sender, args) =>
            {
                playGame.Enabled = false;
                ship.image.BringToFront();
                if (args.Button == MouseButtons.Left)
                {
                    startClick = new Point(args.X, args.Y);
                    initialPosition = ship.image.Location;
                    ship.isClicked = true;
                }
                else if (ship.isClicked && args.Button == MouseButtons.Right)
                {
                    ship.turn = (ship.turn + 1) % 2;
                    ship.image.Image.RotateFlip(RotateFlipType.Rotate90FlipX);
                }
            };
        }

        private void SetShipMouseUp(Ship ship)
        {
            ship.image.MouseUp += (sender, args) =>
            {
                if (args.Button == MouseButtons.Left)
                {
                    ship.isClicked = false;
                    ship.image.Size = ship.image.Image.Size;
                    PlaceShip(ship, initialPosition);
                    ship.turn = 0;
                }
                if (game.player.ships.All(x => IsInField(x.image)))
                    playGame.Enabled = true;
            };
        }

        private void SetShipMouseMove(Ship ship)
        {
            ship.image.MouseMove += (sender, args) =>
            {
                if (ship.isClicked && args.Button == MouseButtons.Left)
                {
                    ship.image.Left += args.X - startClick.X;
                    ship.image.Top += args.Y - startClick.Y;
                }
            };
        }

        private void PlayGame(object sender, EventArgs e)
        {
            Hide();
            new GameForm(game)
            {
                BackgroundImage = Image.FromFile(@"D:\Загрузки\ЯТП2\Metal Sea\images\back.png"),
                BackgroundImageLayout = ImageLayout.Stretch,
                StartPosition = FormStartPosition.CenterScreen,
                ClientSize = new Size(780, 780),
            }.Show();
        }
        private void DrawField(object sender, PaintEventArgs args)
        {
            var g = args.Graphics;
            g.FillRectangle(new SolidBrush(Color.FromArgb(50, Color.Indigo)), 240, 240, 300, 300);
            for (int i = 0; i < 11; i++)
            {
                g.DrawLine(new Pen(Color.FromArgb(150, Color.White), 1), new PointF(240, 240 + i * 30), new PointF(540, 240 + i * 30));
                g.DrawLine(new Pen(Color.FromArgb(150, Color.White), 1), new PointF(240 + i * 30, 240), new PointF(240 + i * 30, 540));
            }
        }

        private bool CheckCollision(PictureBox ship, PictureBox otherShip)
        {
            return ship.Left >= otherShip.Left - 30 && ship.Left < otherShip.Right + 30
                && ship.Top >= otherShip.Top - 30 && ship.Top < otherShip.Bottom + 30
                || ship.Right > otherShip.Left - 30 && ship.Right <= otherShip.Right + 30
                && ship.Bottom > otherShip.Top - 30 && ship.Bottom <= otherShip.Bottom + 30;
        }

        private bool IsInField(PictureBox ship) =>
            ship.Left >= 240 && ship.Left < 540
            && ship.Top >= 240 && ship.Top < 540
            && ship.Right < 540 + 30 
            && ship.Bottom < 540 + 30;

        private void PlaceShip(Ship ship, System.Drawing.Point initialPosition)
        {
            if (IsInField(ship.image))
            {
                ship.image.Left = ship.image.Left / 30 * 30;
                ship.image.Top = ship.image.Top / 30 * 30;
                foreach (var currentShip in game.player.ships.Where(x => !x.image.Equals(ship.image)))
                    if (CheckCollision(ship.image, currentShip.image))
                    {
                        ship.image.Location = initialPosition;
                        if (ship.turn == 1)
                            ship.image.Image.RotateFlip(RotateFlipType.Rotate90FlipX);
                        ship.image.Size = ship.image.Image.Size;
                        break;
                    }
            }
            else
            {
                ship.image.Location = initialPosition;
                if (ship.turn == 1)
                    ship.image.Image.RotateFlip(RotateFlipType.Rotate90FlipX);
                ship.image.Size = ship.image.Image.Size;
            }
        }
    }
}
