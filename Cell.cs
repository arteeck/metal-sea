﻿namespace Metal_Sea
{
    public enum Cell
    {
        Empty,
        Blocked,
        NotActive,
        Occupied,
        Wounded,
        Dead,
        Unknown,
        MissedOnShip
    }
}
